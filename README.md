# post-59-demo

> Support code for https://scvalex.net/posts/59/

This repo contains code to show of implementing Rust's `Iterator`
trait.  The final example shows how to implement an iterator that
mutates itself when the user interacts with the iterator's values.

For instance, this is useful when writing a zero-copy iterator over an
archive's contents (or more generally, when writing a zero-copy
iterator over blocks in a file).

In an ideal world, this would be a "streaming iterator" whose `next()`
function returned mutable references to values stored in the iterator.
We do not live in an ideal world, so streaming iterators aren't a
thing.

See the blog post for more details: https://scvalex.net/posts/59/
