use std::io::Read;

use anyhow::{bail, Error};

fn main() -> Result<(), Error> {
    let mut args = std::env::args();
    match args.nth(1).as_deref() {
        Some("vec") => {
            let vec = vec![100, 200, 300, 400, 500];
            let my_vec = vec::MyVec::new(vec);
            for (idx, x) in my_vec.iter().enumerate() {
                println!("my_vec[{idx}] = {x}");
            }
        }
        Some("blocks1") => {
            let alpha_reader = alphabet_reader::AlphabetReader::new(100);
            let reader = blocks1::MyReader::new(alpha_reader);
            const BLOCK_SIZE: u64 = 10;
            for (idx, block) in reader.blocks(BLOCK_SIZE).enumerate().take(10) {
                let block = block?;
                println!("block {idx}: {}", std::str::from_utf8(&block)?);
            }
        }
        Some("blocks_manual") => {
            let alpha_reader = alphabet_reader::AlphabetReader::new(100);
            let reader = blocks_manual::MyReader::new(alpha_reader);
            const BLOCK_SIZE: u64 = 10;
            const BUF_SIZE: usize = 10;
            let mut idx = 0;
            let mut blocks = reader.blocks(BLOCK_SIZE);
            while idx < 10 {
                match blocks.next() {
                    None => break,
                    Some(mut block) => {
                        let mut buf = [0; BUF_SIZE];
                        block.read_exact(&mut buf)?;
                        println!("block {idx}: {}", std::str::from_utf8(&buf)?);
                    }
                }
                idx += 1;
            }
        }
        Some("blocks2") => {
            let alpha_reader = alphabet_reader::AlphabetReader::new(100);
            let mut reader = blocks2::MyReader::new(alpha_reader);
            const BLOCK_SIZE: u64 = 10;
            const BUF_SIZE: usize = 5;
            for (idx, mut block) in reader.blocks(BLOCK_SIZE).enumerate().take(10) {
                let mut buf = [0; BUF_SIZE];
                block.read_exact(&mut buf)?;
                println!("block {idx}: {}", std::str::from_utf8(&buf)?);
            }
        }
        Some("blocks3") => {
            let alpha_reader = alphabet_reader::AlphabetReader::new(100);
            let mut reader = blocks3::MyReader::new(alpha_reader);
            const BLOCK_SIZE: u64 = 10;
            const BUF_SIZE: usize = 5;
            for (idx, block) in reader.blocks(BLOCK_SIZE).enumerate().take(10) {
                let mut block = block?;
                let mut buf = [0; BUF_SIZE];
                block.read_exact(&mut buf)?;
                println!("block {idx}: {}", std::str::from_utf8(&buf)?);
            }
        }
        Some("blocks4") => {
            let alpha_reader = alphabet_reader::AlphabetReader::new(100);
            let mut reader = blocks4::MyReader::new(alpha_reader);
            const BLOCK_SIZE: u64 = 10;
            const BUF_SIZE: usize = 5;
            for (idx, block) in reader.blocks(BLOCK_SIZE).enumerate() {
                let mut block = block?;
                let mut buf = [0; BUF_SIZE];
                let mut bytes_read = 0;
                while bytes_read < BUF_SIZE {
                    match block.read(&mut buf[bytes_read..])? {
                        0 => break,
                        n => bytes_read += n,
                    }
                }
                println!("block {idx}: {}", std::str::from_utf8(&buf)?);
            }
        }
        Some("blocks_out_of_order") => {
            let alpha_reader = alphabet_reader::AlphabetReader::new(100);
            let mut reader = blocks4::MyReader::new(alpha_reader);
            const BLOCK_SIZE: u64 = 10;
            const BUF_SIZE: usize = 5;
            let mut blocks = reader.blocks(BLOCK_SIZE);
            let mut block1 = blocks.next().unwrap()?;
            let mut block2 = blocks.next().unwrap()?;

            // Read first from block2, then block1
            let mut buf = [0; BUF_SIZE];
            block2.read_exact(&mut buf)?;
            println!("block 2: {}", std::str::from_utf8(&buf)?);
            block1.read_exact(&mut buf)?;
            println!("block 1: {}", std::str::from_utf8(&buf)?);
        }
        Some(cmd) => bail!("unknown command {cmd}"),
        None => bail!("specify one of vec, blocks1, blocks_manual, blocks2, blocks3"),
    }
    Ok(())
}

mod alphabet_reader {
    //! A struct implementing `Read` which generates 'a' for 10 bytes,
    //! then 'b' for 10 bytes, etc.

    use std::io::{self, Read};

    pub struct AlphabetReader {
        idx: u64,
        size: u64,
    }

    impl AlphabetReader {
        pub fn new(size: u64) -> Self {
            Self { idx: 0, size }
        }
    }

    impl Read for AlphabetReader {
        fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
            if self.idx >= self.size {
                Ok(0)
            } else {
                buf[0] = 65 + ((self.idx / 10) % 26) as u8;
                self.idx += 1;
                Ok(1)
            }
        }
    }
}

mod vec {
    //! Iterator over an array of ints.

    pub struct MyVec<T>(Vec<T>);

    pub struct MyIterator<'a, T> {
        vec: &'a MyVec<T>,
        idx: usize,
    }

    impl<T> MyVec<T> {
        pub fn new(vec: Vec<T>) -> Self {
            Self(vec)
        }
        pub fn iter(&self) -> MyIterator<T> {
            MyIterator { vec: self, idx: 0 }
        }
    }

    impl<'a, T> Iterator for MyIterator<'a, T> {
        type Item = &'a T;

        fn next(&mut self) -> Option<Self::Item> {
            if self.idx < self.vec.0.len() {
                self.idx += 1;
                Some(&self.vec.0[self.idx - 1])
            } else {
                None
            }
        }
    }
}

mod blocks1 {
    //! Iterator over fixed-sized blocks of a `impl Read` (e.g. a
    //! `File`).  Each block is read eagerly when `Iterator::next` is
    //! called.

    use std::io::Read;

    pub struct MyReader<R: Read>(R);

    pub struct Blocks<R> {
        reader: R,
        block_size: u64,
    }

    impl<R: Read> MyReader<R> {
        pub fn new(reader: R) -> Self {
            Self(reader)
        }

        pub fn blocks(self, block_size: u64) -> Blocks<R> {
            Blocks {
                reader: self.0,
                block_size,
            }
        }
    }

    impl<R: Read> Iterator for Blocks<R> {
        type Item = Result<Vec<u8>, std::io::Error>;

        fn next(&mut self) -> Option<Self::Item> {
            let mut buf = vec![0; self.block_size as usize];
            let mut bytes_read = 0;
            loop {
                match self.reader.read(&mut buf[bytes_read..]) {
                    Ok(0) => break,
                    Ok(n) => {
                        bytes_read += n;
                        if bytes_read as u64 == self.block_size {
                            break;
                        }
                    }
                    Err(err) => return Some(Err(err)),
                }
            }
            if bytes_read == 0 {
                None
            } else {
                buf.truncate(bytes_read);
                Some(Ok(buf))
            }
        }
    }
}

mod blocks_manual {
    //! Iterator over fixed-sized blocks of a `impl Read` (e.g. a
    //! `File`).  Each block is returned as a `Take` instance that can
    //! be read or ignored by the user.  No care is taken to handle
    //! the case where the user doesn't drain the `Take`.

    use std::io::{Read, Take};

    pub struct MyReader<R: Read> {
        inner: MyReaderInner<R>,
    }

    pub struct MyReaderInner<R> {
        reader: R,
    }

    pub struct Blocks<R> {
        reader: MyReaderInner<R>,
        block_size: u64,
    }

    impl<R: Read> MyReader<R> {
        pub fn new(reader: R) -> Self {
            Self {
                inner: MyReaderInner { reader },
            }
        }

        pub fn blocks(self, block_size: u64) -> Blocks<R> {
            Blocks {
                reader: self.inner,
                block_size,
            }
        }
    }

    impl<R: Read> Read for &mut MyReaderInner<R> {
        fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
            let i = self.reader.read(buf)?;
            Ok(i)
        }
    }

    impl<R: Read> Blocks<R> {
        pub fn next(&mut self) -> Option<Take<&mut MyReaderInner<R>>> {
            // Nothing stop this when it reaches EOF.  It just returns
            // `Take`s with zero bytes in them.
            Some(self.reader.take(self.block_size))
        }
    }
}

mod blocks2 {
    //! Iterator over fixed-sized blocks of a `impl Read` (e.g. a
    //! `File`).  Each block is returned as a `Take` instance that can
    //! be read or ignored by the user.  No care is taken to handle
    //! the case where the user doesn't drain the `Take`.

    use std::{
        cell::RefCell,
        io::{Read, Take},
    };

    pub struct MyReader<R: Read> {
        inner: MyReaderInner<R>,
    }

    pub struct MyReaderInner<R> {
        reader: RefCell<R>,
    }

    pub struct Blocks<'a, R: Read> {
        reader: &'a MyReaderInner<R>,
        block_size: u64,
    }

    impl<R: Read> MyReader<R> {
        pub fn new(reader: R) -> Self {
            Self {
                inner: MyReaderInner {
                    reader: RefCell::new(reader),
                },
            }
        }

        pub fn blocks(&mut self, block_size: u64) -> Blocks<R> {
            Blocks {
                reader: &self.inner,
                block_size,
            }
        }
    }

    impl<'a, R: Read> Read for &'a MyReaderInner<R> {
        fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
            let i = self.reader.borrow_mut().read(buf)?;
            Ok(i)
        }
    }

    impl<'a, R: Read> Iterator for Blocks<'a, R> {
        type Item = Take<&'a MyReaderInner<R>>;

        fn next(&mut self) -> Option<Self::Item> {
            // Nothing stop this when it reaches EOF.  It just returns
            // `Take`s with zero bytes in them.
            Some(self.reader.take(self.block_size))
        }
    }
}

mod blocks3 {
    //! Iterator over fixed-sized blocks of a `impl Read` (e.g. a
    //! `File`).  Each block is returned as a `Take` instance that can
    //! be read or ignored by the user.  Care is taken skip any
    //! remaining bytes in the `Take` if the user doesn't drain it..
    //!
    //! The iterator doesn't properly stop at EOF.

    use std::{
        cell::{Cell, RefCell},
        io::{Read, Take},
    };

    pub struct MyReader<R: Read> {
        inner: MyReaderInner<R>,
    }

    pub struct MyReaderInner<R> {
        reader: RefCell<R>,
        pos: Cell<u64>,
    }

    pub struct Blocks<'a, R: Read> {
        reader: &'a MyReaderInner<R>,
        block_size: u64,
        next: u64,
    }

    impl<R: Read> MyReader<R> {
        pub fn new(reader: R) -> Self {
            Self {
                inner: MyReaderInner {
                    reader: RefCell::new(reader),
                    pos: Cell::new(0),
                },
            }
        }

        pub fn blocks(&mut self, block_size: u64) -> Blocks<R> {
            Blocks {
                reader: &self.inner,
                block_size,
                next: 0,
            }
        }
    }

    impl<'a, R: Read> Read for &'a MyReaderInner<R> {
        fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
            let i = self.reader.borrow_mut().read(buf)?;
            self.pos.set(self.pos.get() + i as u64);
            Ok(i)
        }
    }

    impl<'a, R: Read> Iterator for Blocks<'a, R> {
        type Item = Result<Take<&'a MyReaderInner<R>>, std::io::Error>;

        fn next(&mut self) -> Option<Self::Item> {
            match self.skip_bytes(self.next - self.reader.pos.get()) {
                Ok(()) => {
                    self.next += self.block_size;
                    // Nothing stop this when it reaches EOF.  It just
                    // returns `Take`s with zero bytes in them.
                    Some(Ok(self.reader.take(self.block_size)))
                }
                Err(err) => Some(Err(err)),
            }
        }
    }

    impl<'a, R: Read> Blocks<'a, R> {
        /// Skip bytes by reading them from the `Read`.  This would be
        /// more efficient if we also required `Seek`.
        fn skip_bytes(&mut self, mut bytes_to_skip: u64) -> Result<(), std::io::Error> {
            while bytes_to_skip > 0 {
                let mut buf = [0u8; 4096 * 8];
                let n = std::cmp::min(bytes_to_skip, buf.len() as u64);
                match self.reader.read(&mut buf[..n as usize])? {
                    0 => return Ok(()),
                    n => {
                        bytes_to_skip -= n as u64;
                    }
                }
            }
            Ok(())
        }
    }
}

mod blocks4 {
    //! Iterator over fixed-sized blocks of a `impl Read` (e.g. a
    //! `File`).  Each block is returned as a `Take` instance that can
    //! be read or ignored by the user.  Care is taken skip any
    //! remaining bytes in the `Take` if the user doesn't drain it..
    //!
    //! This the same as block5, but the iterator stops at EOF.

    use std::{
        cell::{Cell, RefCell},
        io::{Read, Take},
    };

    pub struct MyReader<R: Read> {
        inner: MyReaderInner<R>,
    }

    pub struct MyReaderInner<R> {
        reader: RefCell<R>,
        pos: Cell<u64>,
    }

    pub struct Blocks<'a, R: Read> {
        reader: &'a MyReaderInner<R>,
        block_size: u64,
        next: u64,
    }

    impl<R: Read> MyReader<R> {
        pub fn new(reader: R) -> Self {
            Self {
                inner: MyReaderInner {
                    reader: RefCell::new(reader),
                    pos: Cell::new(0),
                },
            }
        }

        pub fn blocks(&mut self, block_size: u64) -> Blocks<R> {
            Blocks {
                reader: &self.inner,
                block_size,
                next: 0,
            }
        }
    }

    impl<'a, R: Read> Read for &'a MyReaderInner<R> {
        fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
            let i = self.reader.borrow_mut().read(buf)?;
            self.pos.set(self.pos.get() + i as u64);
            Ok(i)
        }
    }

    impl<'a, R: Read> Iterator for Blocks<'a, R> {
        type Item = Result<Take<&'a MyReaderInner<R>>, std::io::Error>;

        fn next(&mut self) -> Option<Self::Item> {
            match self.skip_bytes(self.next - self.reader.pos.get()) {
                Ok(0) => {
                    self.next += self.block_size;
                    Some(Ok(self.reader.take(self.block_size)))
                }
                Ok(_) => {
                    // We couldn't skip forward, so we're done.
                    None
                }
                Err(err) => Some(Err(err)),
            }
        }
    }

    impl<'a, R: Read> Blocks<'a, R> {
        /// Skip bytes by reading them from the `Read`.  This would be
        /// more efficient if we also required `Seek`.  Returns the
        /// number of bytes that were NOT skipped (because of EOF or
        /// an error).
        fn skip_bytes(&mut self, mut bytes_to_skip: u64) -> Result<u64, std::io::Error> {
            while bytes_to_skip > 0 {
                let mut buf = [0u8; 4096 * 8];
                let n = std::cmp::min(bytes_to_skip, buf.len() as u64);
                match self.reader.read(&mut buf[..n as usize]) {
                    Ok(0) | Err(_) => return Ok(bytes_to_skip),
                    Ok(n) => {
                        bytes_to_skip -= n as u64;
                    }
                }
            }
            Ok(0)
        }
    }
}
